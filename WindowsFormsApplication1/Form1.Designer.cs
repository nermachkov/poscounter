﻿namespace WindowsFormsApplication1
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.pathText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chboxSubfolders = new System.Windows.Forms.CheckBox();
            this.lblFileCounter = new System.Windows.Forms.Label();
            this.folderOpener = new System.Windows.Forms.FolderBrowserDialog();
            this.btnCounter = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 180);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выберите категорию: ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(60, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 41);
            this.button1.TabIndex = 1;
            this.button1.Text = "Подсчет";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(60, 196);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(146, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.Text = "<Выберите категорию>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Выберите папку с выгрузками: ";
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(226, 18);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFolder.TabIndex = 4;
            this.btnSelectFolder.Text = "Выбрать...";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.button2_Click);
            // 
            // pathText
            // 
            this.pathText.Location = new System.Drawing.Point(60, 47);
            this.pathText.Name = "pathText";
            this.pathText.Size = new System.Drawing.Size(241, 20);
            this.pathText.TabIndex = 5;
            this.pathText.TextChanged += new System.EventHandler(this.pathtext_TextChanged);
            this.pathText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pathText_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(21, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 37);
            this.label3.TabIndex = 6;
            this.label3.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(21, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 37);
            this.label4.TabIndex = 7;
            this.label4.Text = "2";
            // 
            // chboxSubfolders
            // 
            this.chboxSubfolders.AutoSize = true;
            this.chboxSubfolders.Location = new System.Drawing.Point(60, 73);
            this.chboxSubfolders.Name = "chboxSubfolders";
            this.chboxSubfolders.Size = new System.Drawing.Size(121, 17);
            this.chboxSubfolders.TabIndex = 8;
            this.chboxSubfolders.Text = "Включая подпапки";
            this.chboxSubfolders.UseVisualStyleBackColor = true;
            this.chboxSubfolders.CheckedChanged += new System.EventHandler(this.chboxSubfolders_CheckedChanged);
            // 
            // lblFileCounter
            // 
            this.lblFileCounter.AutoSize = true;
            this.lblFileCounter.Location = new System.Drawing.Point(60, 145);
            this.lblFileCounter.Name = "lblFileCounter";
            this.lblFileCounter.Size = new System.Drawing.Size(104, 13);
            this.lblFileCounter.TabIndex = 9;
            this.lblFileCounter.Text = "Найдено: 0 файлов";
            // 
            // btnCounter
            // 
            this.btnCounter.Location = new System.Drawing.Point(60, 96);
            this.btnCounter.Name = "btnCounter";
            this.btnCounter.Size = new System.Drawing.Size(146, 37);
            this.btnCounter.TabIndex = 10;
            this.btnCounter.Text = "Определить категории";
            this.btnCounter.UseVisualStyleBackColor = true;
            this.btnCounter.Click += new System.EventHandler(this.btnCounter_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(264, 96);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(170, 185);
            this.richTextBox1.TabIndex = 11;
            this.richTextBox1.Text = "";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 344);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnCounter);
            this.Controls.Add(this.lblFileCounter);
            this.Controls.Add(this.chboxSubfolders);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pathText);
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "mainForm";
            this.Text = "Подсчет позиций";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.TextBox pathText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chboxSubfolders;
        private System.Windows.Forms.Label lblFileCounter;
        private System.Windows.Forms.FolderBrowserDialog folderOpener;
        private System.Windows.Forms.Button btnCounter;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

