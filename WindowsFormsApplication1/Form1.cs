﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.Common;
using System.Data.SQLite;
using System.Data.OleDb;

namespace WindowsFormsApplication1
{
    public partial class mainForm : Form
    {
        public string tmpPath = System.IO.Path.GetTempPath();
        public string dbName = System.IO.Path.GetTempPath() + @"/poscounter/counterDB.db";
            
        public mainForm()
        {
            InitializeComponent();
        }


        public struct exImport
        {
            public string category, nomenclature;
            public int quantity;
        }
        // загрузка данных и excel файлов и определение доступных категорий.
        private void button1_Click(object sender, EventArgs e)
        {
            // пока что допиливаем кнопку "Определить категории"
            // сначала записываем в базу имеющиеся данные, затем
            // создаем подключение к базе и выгружаем оттуда список всех классов
            // после чего записываем полученные значения в соответствующий элемент формы

            SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0}", dbName));
            connection.Open();
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM 'files';", connection);
            SQLiteDataReader reader = command.ExecuteReader();
            string[] availablefiles = new string[reader.FieldCount + 2];



        }


        // загрузка данных из выбранных файлов (инфа о файлах хранится в sqlite)
        private void loadData()
        {
            // нужно оформить как отдельный класс
            // --------------------------------------------------------------------------------------------------------------
            // подключаемся к базе приложения и получаем список файлов
            SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0}", dbName));
            connection.Open();
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM 'files';", connection);
            SQLiteDataReader reader = command.ExecuteReader();
            string[] availablefiles = new string[reader.FieldCount + 2];





            // файлы нумеруются начиная с 1
            foreach (DbDataRecord record in reader)
            {
                int id = int.Parse(record["id"].ToString());
                string value = record["path"].ToString();
                availablefiles[id] = value;
            }

            // убираем блокировки с файла базы
            reader.Dispose();
            command.Dispose();
            connection.Close();
            connection.Dispose();
            GC.Collect();
            // --------------------------------------------------------------------------------------------------------------

            foreach (string file in availablefiles)
            {
                if (file != null)
                {
                    // и эту часть (возможно в один класс)
                    // --------------------------------------------------------------------------------------------------------------
                    // в excel файлах информация вида:
                    //Категория Номенклатура	Количество
                    //Обувь Ботинки	5
                    //Обувь Кеды	7
                    // создаем переменную для записи в массив
                    int pos = 0;
                    // инициализируем массив записей
                    exImport[] importData = new exImport[100];
                    string con = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=Excel 12.0;", file);
                    using (OleDbConnection exConnection = new OleDbConnection(con))
                    {
                        exConnection.Open();
                        OleDbCommand exCommand = new OleDbCommand("select * from [Лист1$]", exConnection);
                        using (OleDbDataReader dr = exCommand.ExecuteReader())
                        {
                            // считываем данные из таблицы.
                            // dr[0] - Категория
                            // dr[1] - Номенклатура
                            // dr[2] - Количество

                            // считываем построчно документ

                            while (dr.Read())
                            {
                                importData[pos].category = dr[0].ToString();
                                importData[pos].nomenclature = dr[1].ToString();
                                importData[pos].quantity = int.Parse(dr[2].ToString());
                                richTextBox1.Text += importData[pos].category + " " + importData[pos].nomenclature + " " + importData[pos].quantity + "\n";
                                pos++;
                            }
                        }
                        exConnection.Close();
                        exCommand.Dispose();
                        exConnection.Dispose();
                        GC.Collect();
                    }
                    // --------------------------------------------------------------------------------------------------------------
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pathText.Select();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // выбираем папку
            folderOpener.ShowDialog();
            if (folderOpener.SelectedPath!="")
            {
                pathText.Text = folderOpener.SelectedPath;
            }
            pathText.Select();
            countFiles();

        }

        private void countFiles()
        {
            if (folderOpener.SelectedPath != "")
            {
                // Ищем нужные файлы
                Directory.CreateDirectory(tmpPath + @"/poscounter");
                fListCreate(dbName);
                fListFill();
            }
        }
        private void pathtext_TextChanged(object sender, EventArgs e)
        {

        }

        private void pathText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (Directory.Exists(pathText.Text))
                {
                    folderOpener.SelectedPath = pathText.Text;
                }
                else
                {
                    MessageBox.Show("Указанной папки не существует.");
                    pathText.Text = folderOpener.SelectedPath;
                }
        }

        private void btnCounter_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
            loadData();
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (File.Exists(dbName))
            {
                File.Delete(dbName);
                Directory.Delete(tmpPath + @"/poscounter");
            }
           
        }


        // Создаем БД для работы
        private void fListCreate(string basename) {
            // создаем файл с базой
            if (!File.Exists(basename))
            {
                SQLiteConnection.CreateFile(basename);

                // создаем таблицу для списка файлов

                SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0}", basename));
                SQLiteCommand command = new SQLiteCommand("CREATE TABLE files (id INTEGER PRIMARY KEY, path TEXT);", connection);
                connection.Open();
                command.ExecuteNonQuery();
                command.Dispose();

                // закрываем соединение и освобождаем файл базы

                connection.Close();
                command.Dispose();
                connection.Dispose();
                GC.Collect();
            }
            else
            {
                
            }
        }

        // заполняем таблицу списка файлов
        private void fListFill()
        {
            SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0}", dbName));
            connection.Open();
            //перед заполнением очистим текущий список
            string cmd = String.Format("DELETE FROM 'files'");
            SQLiteCommand command = new SQLiteCommand(cmd, connection);
            command.ExecuteNonQuery();
            command.Dispose();
            // счетчик номера
            int i = 0;
            // сначала находим все xlsx и xls файлы
            string supportedExtensions = "*.xls,*.xlsx";
            foreach (string impdata in Directory.GetFiles(folderOpener.SelectedPath, "*.*", chboxSubfolders.Checked ? SearchOption.AllDirectories:SearchOption.TopDirectoryOnly).Where(s => supportedExtensions.Contains(Path.GetExtension(s))))
            {
                    i++;
                    cmd = String.Format("INSERT INTO 'files' ('id', 'path') VALUES ('{0}', '{1}');", i, impdata);
                    command = new SQLiteCommand(cmd, connection);
                    command.ExecuteNonQuery();
                    command.Dispose();
                    lblFileCounter.Text = " Файлов подготовлено к импорту: " + i;
             
            }
            // закрываем соединение и убираемся
            connection.Close();
            connection.Dispose();
            GC.Collect();
        }

        private void chboxSubfolders_CheckedChanged(object sender, EventArgs e)
        {
            countFiles();
        }

    }
}
